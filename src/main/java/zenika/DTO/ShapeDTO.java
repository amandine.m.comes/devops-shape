package zenika.DTO;

import zenika.application.Shape;

public class ShapeDTO {
    private final Shape shape;
    private final int red;
    private final int green;
    private final int blue;

    public ShapeDTO(Shape shape, int r, int g, int b){
        this.shape = shape;
        this.red = r;
        this.green = g;
        this.blue = b;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public Shape getShape() {
        return shape;
    }
}
