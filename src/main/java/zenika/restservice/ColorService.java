package zenika.restservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zenika.DTO.ColorDTO;

@Service
public class ColorService {
    private final RestTemplate restTemplate;
    private final String url;

    public ColorService(RestTemplateBuilder restTemplateBuilder,
                        @Value("${server.servlet.url}")String url){
        this.restTemplate = restTemplateBuilder.build();
        this.url = url;
        System.out.println(url);
    }

    public ColorDTO getRandomColorFromApi(){
        return this.restTemplate.getForObject(this.url, ColorDTO.class);
    }


}
